package com.gradientx.thrift;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.thrift.TServiceClient;
import org.apache.thrift.TServiceClientFactory;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;

/**
 * @author dgoya
 *
 */
public abstract class ThriftClientPool <T extends TServiceClient>{
	
	/**
	 * The Map which contains our connection pools per socket address
	 */
	private final Map<InetSocketAddress, List<T>> clientPoolMap;
	
	private final TServiceClientFactory<T> clientFactory;

	/**
	 * Cannot be created
	 * @throws IOException 
	 */
	public ThriftClientPool() {
		clientPoolMap = new HashMap<InetSocketAddress, List<T>>();
		clientFactory = createFactory();
	}
	
	protected abstract TServiceClientFactory<T> createFactory();

	/**
	 * Get a socket from a connection pool if one exists, otherwise create a new one
	 * 
	 * @param socketAddress
	 * @param timeout
	 * @return
	 * @throws TTransportException
	 */
	public synchronized T getClient(
			final InetSocketAddress socketAddress) throws IOException, TTransportException {
		List<T> clientPool = clientPoolMap.get(socketAddress);
		
		if(clientPool == null){
			clientPool = new ArrayList<T>();
			clientPoolMap.put(socketAddress, clientPool);
		}
		
		if(clientPool.size() > 0){
			T client = clientPool.remove(0);
			return client;
		}else{
			TSocket tSocket = new TSocket(
					socketAddress.getHostString(), 
					socketAddress.getPort());
			TFramedTransport framedTransport = new TFramedTransport(tSocket);
			framedTransport.open();
			TCompactProtocol compactProtocol = new TCompactProtocol(framedTransport);
			return clientFactory.getClient(compactProtocol);
		}
	}
	
	/**
	 * Release the connection back to the pool
	 * 
	 * @param tSocket
	 */
	public synchronized void releaseClient(InetSocketAddress destinationAddress, T client){
		List<T> clientPool = clientPoolMap.get(destinationAddress);
		
		if(clientPool == null){
			clientPool = new ArrayList<T>();
		}
		
		clientPool.add(client);
	}
}
