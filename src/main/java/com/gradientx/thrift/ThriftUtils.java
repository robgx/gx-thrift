package com.gradientx.thrift;

import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TEnum;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransportException;

public class ThriftUtils {

	private ThriftUtils() {}

	public static ServerSocket getServerSocketFor(TNonblockingServerSocket thriftSocket)
			throws TTransportException {
		try {
			Field field = TNonblockingServerSocket.class.getDeclaredField("serverSocket_");
			field.setAccessible(true);
			return (ServerSocket) field.get(thriftSocket);
		} catch (NoSuchFieldException e) {
			throw new TTransportException("Couldn't get listening port", e);
		} catch (SecurityException e) {
			throw new TTransportException("Couldn't get listening port", e);
		} catch (IllegalAccessException e) {
			throw new TTransportException("Couldn't get listening port", e);
		}
	}
	
	public static List<String> enumListToStringList(List<? extends TEnum> enumList){
		List<String> retVal = new ArrayList<String>();
		for(TEnum tEnum: enumList){
			retVal.add(Integer.toString(tEnum.getValue()));
		}
		return retVal;
	}
}
