package com.gradientx.thrift;

import java.io.IOException;

import org.apache.thrift.async.TAsyncClientFactory;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TCompactProtocol;

import com.gradientx.prediction.BidGenerationService;

/**
 * @author dgoya
 *
 */
public class BidGenerationServiceAsyncClientPool extends ThriftAsyncClientPool<BidGenerationService.AsyncClient> {

	public BidGenerationServiceAsyncClientPool(int maxInactiveSize, int maxOutstandingSize) throws IOException {
		super("BidGenerationServiceAsyncClientPool", maxInactiveSize, maxOutstandingSize);
	}

	@Override
	protected TAsyncClientFactory<BidGenerationService.AsyncClient> createFactory() throws IOException {
		return new BidGenerationService.AsyncClient.Factory(new TAsyncClientManager(), new TCompactProtocol.Factory());
	}
}
