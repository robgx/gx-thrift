package com.gradientx.thrift;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.thrift.async.TAsyncClient;
import org.apache.thrift.async.TAsyncClientFactory;
import org.apache.thrift.transport.TNonblockingSocket;
import org.apache.thrift.transport.TTransportException;

import com.gradientx.utils.GxStat;

/**
 * @author dgoya
 *
 */
public abstract class ThriftAsyncClientPool <T extends TAsyncClient>{
	
	private class TimestampedSocket {
		public final TNonblockingSocket socket;
		public final Long timestamp;
		
		public TimestampedSocket(TNonblockingSocket socket){
			this.socket = socket;
			this.timestamp = System.currentTimeMillis();
		}
	}
	
	/**
	 * The Map which contains our connection pools per socket address
	 */
	private final Map<InetSocketAddress, List<T>> clientPoolMap;
	
	private final Map<T, TimestampedSocket> clientSocketMap;
	
	private final TAsyncClientFactory<T> clientFactory;
	
	private final GxStat inactivePoolStat;
	private final GxStat outstandingPoolStat;
	
	private int currentOutstanding;
	
	private final int maxInactiveSize;
	private final int maxOutstandingSize;
	
	private static final Logger logger = Logger.getLogger(ThriftAsyncClientPool.class);
	
	private static final long maxAge = 5000l;

	/**
	 * Cannot be created
	 * @throws IOException 
	 */
	public ThriftAsyncClientPool(
			String poolName, 
			int maxInactiveSize,
			int maxOutstandingSize) throws IOException {
		clientPoolMap = new HashMap<InetSocketAddress, List<T>>();
		clientSocketMap = new HashMap<T, TimestampedSocket>();
		clientFactory = createFactory();
		inactivePoolStat = new GxStat(poolName + " : inactive");
		outstandingPoolStat = new GxStat(poolName + " : outstanding");
		this.maxInactiveSize = maxInactiveSize;
		this.maxOutstandingSize = maxOutstandingSize;
		currentOutstanding = 0;
	}
	
	protected abstract TAsyncClientFactory<T> createFactory() throws IOException;

	/**
	 * Get a socket from a connection pool if one exists, otherwise create a new one
	 * 
	 * @param socketAddress
	 * @param timeout
	 * @return
	 * @throws TTransportException
	 */
	public synchronized T getClient(
			final InetSocketAddress socketAddress) throws IOException, TTransportException {
		List<T> clientPool = clientPoolMap.get(socketAddress);
		
		if(clientPool == null){
			clientPool = new ArrayList<T>();
			clientPoolMap.put(socketAddress, clientPool);
		}
		
		int inactivePoolSize = clientPool.size();
		inactivePoolStat.add(inactivePoolSize);
		inactivePoolStat.logAndResetAtCheckPoint(logger, Level.WARN, 10000);
		outstandingPoolStat.add(currentOutstanding);
		outstandingPoolStat.logAndResetAtCheckPoint(logger, Level.WARN, 10000);
		if(currentOutstanding < maxOutstandingSize){
			currentOutstanding++;
			if(inactivePoolSize > 0){
				T client = clientPool.remove(0);
				return client;
			}else{
				TNonblockingSocket tSocket = new TNonblockingSocket(
						socketAddress.getHostString(), 
						socketAddress.getPort());
				T client = clientFactory.getAsyncClient(tSocket);
				clientSocketMap.put(client, new TimestampedSocket(tSocket));
				return client;
			}
		}else{
			return null;
		}
	}
	
	public synchronized void destroyClient(T client){
		currentOutstanding--;
		TimestampedSocket timestampedSocket = clientSocketMap.remove(client);
		if(timestampedSocket != null){
			timestampedSocket.socket.close();
		}
	}
	
	/**
	 * Release the connection back to the pool
	 * 
	 * @param tSocket
	 */
	public synchronized void releaseClient(InetSocketAddress destinationAddress, T client){
		currentOutstanding--;
		List<T> clientPool = clientPoolMap.get(destinationAddress);
		
		if(clientPool == null){
			clientPool = new ArrayList<T>();
		}
		
		if(clientPool.size() > maxInactiveSize){
			//too many inactive
			TimestampedSocket timestampedSocket = clientSocketMap.remove(client);
			if(timestampedSocket != null){
				timestampedSocket.socket.close();
			}
		}else{
			clientPool.add(client);
		}
		
		if(currentOutstanding >= maxOutstandingSize){
			//trim the pool
			List<T> clientsToRemove = new ArrayList<T>();
			for(Map.Entry<T, TimestampedSocket> entry: clientSocketMap.entrySet()){
				TimestampedSocket timestampedSocket = entry.getValue();
				if(System.currentTimeMillis() - timestampedSocket.timestamp > ThriftAsyncClientPool.maxAge){
					clientsToRemove.add(entry.getKey());
				}
			}
			
			for(T clientToRemove: clientsToRemove){
				TimestampedSocket socketToClose = clientSocketMap.remove(clientToRemove);
				socketToClose.socket.close();
				currentOutstanding--;
			}
		}
	}
}
