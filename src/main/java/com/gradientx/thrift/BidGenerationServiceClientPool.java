package com.gradientx.thrift;

import org.apache.thrift.TServiceClientFactory;

import com.gradientx.prediction.BidGenerationService;

/**
 * @author dgoya
 *
 */
public class BidGenerationServiceClientPool extends ThriftClientPool<BidGenerationService.Client> {

	@Override
	protected TServiceClientFactory<BidGenerationService.Client> createFactory() {
		return new BidGenerationService.Client.Factory();
	}
}
