package com.gradientx.thrift;

import org.apache.thrift.TServiceClientFactory;

import com.gradientx.targeting.TargetingService;

public class TargetingServiceClientPool extends ThriftClientPool<TargetingService.Client> {

	@Override
	protected TServiceClientFactory<TargetingService.Client> createFactory() {
		return new TargetingService.Client.Factory();
	}
}
