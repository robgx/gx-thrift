package com.gradientx.thrift;

import java.util.Date;

import com.gradientx.common.UnitUtils;
import com.gradientx.common.thrift.DeliveryStats;
import com.gradientx.common.thrift.FlightDates;
import com.gradientx.common.thrift.ItemInfo;

public class ItemInfoWrapper {
	
	private final ItemInfo itemInfo;
	
	public ItemInfoWrapper(ItemInfo itemInfo){
		this.itemInfo = itemInfo;
	}
	
	/**
	 * Get how much of the budget we have consumed in total
	 * 
	 * @return
	 */
	public double getConsumedBudgetInUsd(boolean daily) {
		if(daily){
			return UnitUtils.microsToUsd(
					itemInfo.getDeliveryStats().getDailySpentInMicros() +
					itemInfo.getDeliveryStats().getPotentialSpentInMicros());
		}else{
			return UnitUtils.microsToUsd(
					itemInfo.getDeliveryStats().getTotalSpentInMicros() +
					itemInfo.getDeliveryStats().getPotentialSpentInMicros());
		}
	}

	/**
	 * Get the remaining budget, either daily or total
	 * 
	 * @return
	 */
	public double getRemainingBudgetInUsd() {
		return itemInfo.getTotalSpendInUsd() - getConsumedBudgetInUsd(false);
	}
	
	/**
	 * Get the values for how much we have delivered in total
	 * 
	 * @return
	 */
	public long getConsumedUnits(boolean daily) {
		DeliveryStats deliveryStats = itemInfo.getDeliveryStats();
		switch(itemInfo.getPricingType()){
		case cpm:
			if(daily){
				return deliveryStats.getDailyImpressions();
			}else{
				return deliveryStats.getTotalImpressions();
			}
		case cpc:
			if(daily){
				return deliveryStats.getDailyClicks();
			}else{
				return deliveryStats.getTotalClicks();
			}
		case cpa:
		case cpi:
			if(daily){
				return deliveryStats.getDailyConversions();
			}else{
				return deliveryStats.getTotalConversions();
			}
		default:
			return 0;
		}
	}

	/**
	 * Get the number of units left to deliver
	 * 
	 * @return
	 */
	public double getRemainingUnits() {
		return itemInfo.getTotalUnits() - getConsumedUnits(false);
	}
	
	public boolean isActive(Date nowDate) {
		//check if we are in the flight
		FlightDates flightDate = itemInfo.getFlightDates();
		long startTime = itemInfo.getFlightStart();
		long nowTime = nowDate.getTime();
		if(nowTime < startTime){
			return false;
		}else{
			switch(flightDate){
			case fixed:
				long endTime = itemInfo.getFlightEnd();
				if(nowTime > endTime){
					return false;
				}
				break;
			case open:
				break;
			}
		}
		
		//check if the item is spend capped
		double dailySpendCap = itemInfo.getDailySpendInUsd();
		if(dailySpendCap > 0){
			double dailySpent = getConsumedBudgetInUsd(true);
			if(dailySpent >= dailySpendCap){
				return false;
			}
		}
		
		//check if the item is unit capped
		long dailyUnitCap = itemInfo.getDailyUnits();
		if(dailyUnitCap > 0){
			long dailyUnits = getConsumedUnits(true);
			if(dailyUnits >= dailyUnitCap){
				return false;
			}
		}
		
		//check if the item is completed by spend
		double totalSpend = itemInfo.getTotalSpendInUsd();
		if(totalSpend > 0){
			double deliveredSpend = getConsumedBudgetInUsd(false);
			if(deliveredSpend >= totalSpend){
				return false;
			}
		}
		
		//check if the item is completed by units
		long totalUnits = itemInfo.getTotalUnits();
		if(totalUnits > 0){
			long deliveredUnits = getConsumedUnits(false);
			if(deliveredUnits >= totalUnits){
				return false;
			}
		}
		
		return true;
	}
}
