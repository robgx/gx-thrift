package com.gradientx.thrift;

import java.io.IOException;

import org.apache.thrift.async.TAsyncClientFactory;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TCompactProtocol;

import com.gradientx.targeting.TargetingService;
import com.gradientx.thrift.ThriftAsyncClientPool;

/**
 * 
 * @author dapeng
 *
 */
public class TargetingServiceAsyncClientPool extends ThriftAsyncClientPool<TargetingService.AsyncClient> {

	public TargetingServiceAsyncClientPool(int maxInactiveSize, int maxOutstandingSize) throws IOException {
		super("TargetingServiceAsyncClientPool",  maxInactiveSize, maxOutstandingSize);
	}

	@Override
	protected TAsyncClientFactory<TargetingService.AsyncClient> createFactory() throws IOException {
		return new TargetingService.AsyncClient.Factory(new TAsyncClientManager(), new TCompactProtocol.Factory());
	}
}
