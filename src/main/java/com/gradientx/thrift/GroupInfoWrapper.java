package com.gradientx.thrift;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TCompactProtocol;

import com.gradientx.common.DayTime;
import com.gradientx.common.TimeUtils;
import com.gradientx.common.UnitUtils;
import com.gradientx.common.thrift.DeliveryStats;
import com.gradientx.common.thrift.FlightDates;
import com.gradientx.common.thrift.GroupInfo;
import com.gradientx.common.thrift.GroupStatus;
import com.gradientx.common.thrift.GroupStatusInfo;
import com.gradientx.common.thrift.ItemInfo;
import com.gradientx.common.thrift.Pacing;
import com.gradientx.common.thrift.PricingType;

public class GroupInfoWrapper {
	
	private final ItemInfo itemInfo;
	private final GroupInfo groupInfo;
	private final DayTime daytime;
	
	private static ThreadLocal<TSerializer> serializer = new ThreadLocal<TSerializer>(){
		protected TSerializer initialValue() {
			return new TSerializer(new TCompactProtocol.Factory());
		};
	};
	
	private static ThreadLocal<TDeserializer> deserializer = new ThreadLocal<TDeserializer>(){
		protected TDeserializer initialValue() {
			return new TDeserializer(new TCompactProtocol.Factory());
		};
	};
	
	public GroupInfoWrapper(byte[] itemInfoBytes, byte[] groupInfoBytes) throws TException, ParseException{
		itemInfo = new ItemInfo();
		deserializer.get().deserialize(itemInfo, itemInfoBytes);
		
		groupInfo = new GroupInfo();
		deserializer.get().deserialize(groupInfo, groupInfoBytes);
		
		List<String> dayTimeTargeting = groupInfo.getDayTime();
		if(dayTimeTargeting == null || dayTimeTargeting.size() == 0){
			daytime = new DayTime(null);
		}else{
			daytime = new DayTime(groupInfo.getDayTime());
		}
	}
	
	public GroupInfoWrapper(ItemInfo itemInfo, GroupInfo groupInfo) throws ParseException {
		this.itemInfo = itemInfo;
		this.groupInfo = groupInfo;
		
		List<String> dayTimeTargeting = groupInfo.getDayTime();
		if(dayTimeTargeting == null || dayTimeTargeting.size() == 0){
			daytime = new DayTime(null);
		}else{
			daytime = new DayTime(groupInfo.getDayTime());
		}
	}

	public GroupInfo getGroupInfo(){
		return groupInfo;
	}

	public ItemInfo getItemInfo() {
		return itemInfo;
	}
	
	/**
	 * @return the dayTime
	 */
	public DayTime getDayTime() {
		return daytime;
	}

	/**
	 * Does this line item have a daily budget?
	 * 
	 * @return
	 */
	public boolean useDailyBudget() {
		if(itemInfo.isSetFlightDates()
				&& itemInfo.getFlightDates().equals(FlightDates.open)
				&& itemInfo.getPacing().equals(Pacing.even)){
			return true;
		}else{
			return false;
		}
	}
	
	public double getCatchupTimeMultiplier(
			Date nowDate, 
			Date endOfDayUtc, 
			boolean daily, 
			double pacingControlAlpha){
		long totalUnits = getTotalUnits(daily);
		double totalBudget = getTotalBudget(daily);
		
		long deliveredUnits = getConsumedUnits(daily);
		double deliveredBudget = getConsumedBudget(daily);
		
		long remainingUnits = totalUnits - deliveredUnits;
		double remainingBudget = totalBudget - deliveredBudget;
		
		long remainingTime;
		if(daily){
			remainingTime = endOfDayUtc.getTime() - nowDate.getTime();
		}else{
			remainingTime = itemInfo.getFlightEnd() - nowDate.getTime();
		}
		
		long totalTime;
		if(daily){
			totalTime = TimeUtils.dayInMillis;
		}else{
			totalTime = itemInfo.getFlightEnd() - itemInfo.getFlightStart();
		}
		
		if(pacingControlAlpha < 1.0 && pacingControlAlpha > 0 && remainingTime > 0){
			if(totalUnits > 0){
			    double scheduledRemaining = totalUnits * ((double)remainingTime/(double)totalTime);
			    double deliveryError = scheduledRemaining - remainingUnits;
			    double deliveryErrorWithAlpha = deliveryError / pacingControlAlpha;
			    double retVal = (scheduledRemaining + deliveryErrorWithAlpha) / scheduledRemaining;
			    if(retVal < 0.0){
			    	return 0.0;
			    }else{
			    	return retVal;
			    }
			}else{
				double scheduledRemaining = totalBudget * ((double)remainingTime/(double)totalTime);
			    double deliveryError = scheduledRemaining - remainingBudget;
			    double deliveryErrorWithAlpha = deliveryError / pacingControlAlpha;
			    double retVal = (scheduledRemaining + deliveryErrorWithAlpha) / scheduledRemaining;
			    if(retVal < 0.0){
			    	return 0.0;
			    }else{
			    	return retVal;
			    }
			}
		}else{
			return 1.0;
		}
	}
	
	public double getTotalBudget(boolean daily){
		if(daily){
			return groupInfo.getDailySpendInUsd();
		}else{
			return groupInfo.getTotalSpendInUsd();
		}
	}
	
	/**
	 * Get how much of the budget we have consumed, either daily or total
	 * 
	 * @return
	 */
	public double getConsumedBudget() {
		return getConsumedBudget(useDailyBudget());
	}

	public double getConsumedBudget(boolean daily) {
		DeliveryStats deliveryStats = groupInfo.getDeliveryStats();
		if(daily){
			return UnitUtils.microsToUsd(
					deliveryStats.getDailySpentInMicros() + 
					deliveryStats.getPotentialSpentInMicros());
		}else{
			return UnitUtils.microsToUsd(
					deliveryStats.getTotalSpentInMicros() +
					deliveryStats.getPotentialSpentInMicros());
		}
	}

	/**
	 * Get the remaining budget, either daily or total
	 * 
	 * @return
	 */
	public double getRemainingBudget() {
		return getRemainingBudget(useDailyBudget());
	}
	
	public double getRemainingBudget(boolean daily){
		return getTotalBudget(daily) - getConsumedBudget(daily);
	}

	/**
	 * Get the number of units to deliver, either daily or total
	 * 
	 * @return
	 */
	public double getTotalUnits() {
		return getTotalUnits(useDailyBudget());
	}
	
	public long getTotalUnits(boolean daily){
		if(daily){
			return groupInfo.getDailyUnits();
		}else{
			return groupInfo.getTotalUnits();
		}
	}
	
	/**
	 * Get the values for how much we have delivered, either daily or total
	 * 
	 * @return
	 */
	public double getConsumedUnits() {
		return getConsumedUnits(useDailyBudget());
	}
	
	public long getConsumedUnits(boolean daily){
		DeliveryStats deliveryStats = groupInfo.getDeliveryStats();
		long count = 0;
		
		PricingType ty = itemInfo.getPricingType();
		switch(ty){
		case cpm:
			count = deliveryStats.getDailyImpressions();
			if (!daily) count = deliveryStats.getTotalImpressions();
			break;
		case cpc:
			count = deliveryStats.getDailyClicks();
			if (!daily) count = deliveryStats.getTotalClicks();
			break;
		case cpi:
		case cpa:
			count = deliveryStats.getDailyConversions();
			if (!daily) count = deliveryStats.getTotalConversions();
			break;
		}
		
		return count;
	}

	/**
	 * Get the number of units left to deliver
	 * 
	 * @return
	 */
	public double getRemainingUnits() {
		return getRemainingUnits(useDailyBudget());
	}
	
	public double getRemainingUnits(boolean daily){
		return getTotalUnits(daily) - getConsumedUnits(daily);
	}
	
	public boolean isActive(Date nowDate) {
		ItemInfoWrapper itemInfoWrapper = new ItemInfoWrapper(itemInfo);
		if(itemInfoWrapper.isActive(nowDate) == false){
			return false;
		}
		
		//make sure the group is active/live
		if(groupInfo.getGroupStatus() != GroupStatus.active
				|| groupInfo.getGroupStatusInfo() != GroupStatusInfo.live){
			return false;
		}
		
		//check if the group is spend capped
		double dailySpendCap = groupInfo.getDailySpendInUsd();
		if(dailySpendCap > 0){
			double dailySpent = getConsumedBudget(true);
			if(dailySpent >= dailySpendCap){
				return false;
			}
		}
		
		//check if the group is unit capped
		long dailyUnitCap = groupInfo.getDailyUnits();
		if(dailyUnitCap > 0){
			long dailyUnits = getConsumedUnits(true);
			if(dailyUnits >= dailyUnitCap){
				return false;
			}
		}
		
		//check if the group is completed by spend
		double totalSpend = groupInfo.getTotalSpendInUsd();
		if(totalSpend > 0){
			double deliveredSpend = getConsumedBudget(false);
			if(deliveredSpend >= totalSpend){
				return false;
			}
		}
		
		//check if the group is completed by units
		double totalUnits = groupInfo.getTotalUnits();
		if(totalUnits > 0){
			long deliveredUnits = getConsumedUnits(false);
			if(deliveredUnits >= totalUnits){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Get the adjusted average bid price
	 * 
	 * @param in
	 * @param aveBidPrice
	 * @return
	 */
	public double getFutureAverage(double currentAverage) {
		double futureAve = 0.0;

		// optimize by budget (not by units).
		if (groupInfo.isSetTotalSpendInUsd() && groupInfo.getTotalSpendInUsd() > 0) {
			// since the spend is given, total unit is not available.
			double futureUnits = (getTotalBudget(false) / currentAverage) - getConsumedUnits(false);
			double remainingBudget = getRemainingBudget(false);
			if(remainingBudget > 0 && futureUnits <= 0){
				return currentAverage;
			}else{
				futureAve = remainingBudget / futureUnits;
			}
		} else {
			// the total unit is given, so the total spend is not given.
			double futureBudget = (getTotalUnits(false) * currentAverage) - getConsumedBudget(false);
			double remainingUnits = getRemainingUnits(false);
			if(remainingUnits > 0){
				futureAve = futureBudget / remainingUnits;
			}else{
				futureAve = 0.0;
			}
		}
		
		// clip by x10 of original
		double maxLimit = currentAverage * 10;
		if (futureAve > maxLimit) futureAve = maxLimit;

		// set a floor of 10%
		double minLimit = currentAverage * 0.1;
		if (futureAve < minLimit) futureAve = minLimit;
		
		return futureAve;
	}
	
	public double getFutureAvgBidPrice(double bidPriceCpmUsd){
		//shift down to units
		double futureAvgBidPrice = getFutureAverage(bidPriceCpmUsd / 1000d);
		//shift back from units to CPM
		return futureAvgBidPrice * 1000;
	}
	
	public double getFutureGoal(double goalPrice) {
		double futureAvgBidPrice = getFutureAverage(goalPrice);
		return futureAvgBidPrice;
	}
	
	public byte[] getGroupInfoBytes() throws TException{
		return serializer.get().serialize(groupInfo);
	}
	
	public byte[] getItemInfoBytes() throws TException{
		return serializer.get().serialize(itemInfo);
	}
	
	public double getGoalPrice(){
		if(groupInfo.isOverride_set() && groupInfo.getGoalPriceOverride() > 0){
			return groupInfo.getGoalPriceOverride();
		}else{
			return itemInfo.getGoalPriceInUsd();
		}
	}
}
