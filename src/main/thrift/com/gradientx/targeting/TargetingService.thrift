namespace java com.gradientx.targeting

include "com/gradientx/gateway/Gateway.thrift"
include "com/gradientx/userdata/UserData.thrift"

service TargetingService {
	list<Gateway.AdKey> targetAd (1: string requestId, 2: binary bidResponseEvent, 3: UserData.UserData userData)
}
