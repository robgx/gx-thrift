namespace java com.gradientx.opt

include "com/gradientx/common/GradientX.thrift" 

enum OptimizerFeedback {
    Inactive,
    PacingFast,
    ReducedTrackingBidRate,
    OptionalCTRGoalTooHigh,
    OptionalCPAGoalTooLow,
    OptionalCPMGoalTooLow,
    UnableToDeliverAtGoal,
    ReducedDeliveryBidRate,
    MinimumPriceResoution,
    MaximumSearchIterations,
    OptimalSolutionFound,
    CatchingUpAsFastAsPossible
}

struct OptimizerResult {
    1: double price;
    2: double bidRate;
    3: double ctrThreshold;
    4: double cpaThreshold;
    5: double maxBid;
    6: bool discountBidByFee;
    7: i64 requests;
    8: i64 wins;
    9: i64 clicks;
    10: i64 actions;
    11: double spent;
    12: double averageCtr;
    13: double stdevCtr;
    14: double averageConvRate;
    15: double stdevConvRate;
    16: list<OptimizerFeedback> feedback;
    17: GradientX.PricingType pricingType;
    18: double effectivePriceGoal;
}