namespace java com.gradientx.dmp

enum EventType {
	IMPRESSION,
	CLICK,
	CONVERSION,
	OPTOUT,
	DPID_SEGMENT_ADD,
	DPID_SEGMENT_DEL,
	SEGMENT_ADD,
	SEGMENT_DEL
}

struct Event {
    01: EventType type,
	02: string id,
	10: optional i32 itemId,
	20: optional list<string> languages
	21: optional list<string> segments
	22: optional i32 ttl
}