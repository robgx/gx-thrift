namespace java com.gradientx.prediction

include "com/gradientx/gateway/Gateway.thrift" 
include "com/gradientx/userdata/UserData.thrift"

service BidGenerationService {
    map<Gateway.AdKey, Gateway.Bid> generateBid(1: string id, 2: binary binaryEvent, 3: list<Gateway.AdKey> opportunities, 4: UserData.UserData userData)
    void gotResult(1: string id, 2:map<Gateway.AdKey, Gateway.Bid> resultMap)
}
