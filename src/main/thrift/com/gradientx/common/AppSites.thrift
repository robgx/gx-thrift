namespace java com.gradientx.common.thrift

struct AppSite {
    1: required string appsite_id;
    2: required string site_id;
    3: required string app_id;
    4: required string category_code;
    5: required string subcategory_code;
    6: optional bool blind = false;
    7: optional bool blocked = false;
    8: optional string app_content_code;
    9: optional string app_rating_code;
    10: optional string app_price_code;
    11: optional string publisher_id;
}

struct AppSiteMap {
    1: map<string, AppSite> appsites;
}