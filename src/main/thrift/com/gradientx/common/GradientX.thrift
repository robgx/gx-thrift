namespace java com.gradientx.common.thrift

struct TQEntry {
	1: string requestId;
	2: i64 timestamp;
}

enum Partner {
    MobClix = 1001,
    MoPub = 1002,
    Nexage = 1003,
    Smaato = 1004,
    Flurry = 1005,
    Brightroll = 1006,
    Rubicon = 1007,
    OpenX = 1008,
    AdapTV = 1009,
    AdExchange = 1010,
    Amobee = 1011
}

enum CreativeType {
    banner,
    video,
}

enum CreativeSubType {
    text = 1,
    banner = 2,
    javascript = 3,
    iframe = 4
}

enum ExpandableDirection {
    left = 1,
    right = 2,
    up = 3,
    down = 4
}

enum CreativeAttributes {
    AudioAdAutoPlay = 1,
    AudioAdUserInitiated = 2,
    ExpandableAutomatic = 3,
    ExpandableUserInitiatedClick = 4,
    ExpandableUserInitiatedRollover = 5,
    InBannerVideoAdAutoPlay = 6,
    InBannerVideoAdUserInitiated = 7,
    Pop = 8,
    ProvocativeSuggestiveImagery = 9,
    ShakyFlashingFlickeringExtremeAnimationSmileys = 10,
    Surveys = 11,
    TextOnly = 12,
    UserInteractive = 13,
    WindowsDialogAlertStyle = 14,
    HasAudioOnOffButton = 15,
    AdCanBeSkipped = 16
}

enum ApiFrameworks {
    VPAID1 = 1,
    VPAID2 = 2,
    MRAID = 3,
    ORMMA = 4,
    MobileWeb = 9,
}

enum Linearity {
    Linear = 1,
    NonLinear = 2
}

enum VideoProtocols {
    VAST1 = 1,
    VAST2 = 2,
    VAST3 = 3,
    VAST1WRAPPER = 4,
    VAST2WRAPPER = 5,
    VAST3WRAPPER = 6
}

enum GroupStatus {
    done,
    draft,
    active,
    paused,
    deleted
}

enum GroupStatusInfo {
    draft,
    processing,
    waiting,
    pending,
    rejected,
    live,
    notserving,
    capped,
    pausing,
    paused,
    stopping,
    stopped,
    completed,
    expired,
    deleted
}

enum DealType {
    banner,
    video,
    desktop_banner,
    desktop_video
}

enum PricingType {
    cpm,
    cpc,
    cpa,
    cpi,
}

enum FlightDates {
    fixed,
    open
}

enum Pacing {
    fast,
    smooth,
    even
}

enum PacingControl {
    fast,
    medium,
    slow,
    none
}

enum DeliveryAllocation {
    manual,
    auto
}

struct FeeParameters {
    1: double feePower = 2.0;
    2: double curveMinPrice = 0.05;
    3: double curveMinFee = 0.02;
    4: double curveMaxPrice = 10.00;
    5: double curveMaxFee = 1.00;
    6: double flatRate = 0.10;
    7: double feeLimitMin = 0.02;
    8: double feeLimitMax = 5.00;
}

struct DeliveryStats {
    1: i64 totalSpentInMicros;
    2: i64 dailySpentInMicros;
    3: i64 potentialSpentInMicros;
    
    4: i32 totalImpressions;
    5: i32 dailyImpressions;
    6: i32 totalClicks;
    7: i32 dailyClicks;
    8: i32 totalConversions;
    9: i32 dailyConversions;
}

struct Lander {
    1: string id;
    2: string name;
    3: string creativeId;
    
    4: string url;
    5: string landingPageUrl;
    6: string IABCategory;
}

struct Creative {
    1: string id;
    2: string name;
    3: string groupId;
    4: CreativeType type;
    
    //common stuff
    5: string markup;
    6: string language;
    7: string mimeType;
    8: list<CreativeAttributes> attributes;
    9: list<ApiFrameworks> requiredFrameworks;
    10: string iUrl;
    11: map<string, Lander> landers;
    
    //banner stuff
    12: list<CreativeSubType> subTypes;
    13: string dimensions;
    14: string adServerId;
    19: set<ExpandableDirection> expDir;
    
    //video stuff
    15: VideoProtocols videoProtocol;
    16: i32 duration;
    17: i32 minBitrate;
    18: i32 maxBitrate;

    //partners this creative can serve on
    20: list<Partner> approvedPartners;
}

struct GroupInfo {
    1: string id;
    2: string name;
    3: string itemId;
    
    4: i64 allocatePercent;
    5: i64 groupDailySpendInUsd;
    
    6: list<string> dayTime;
    7: string targeting;
    
    8: GroupStatus groupStatus;
    9: GroupStatusInfo groupStatusInfo;
    
    10: i64 totalUnits;
    11: i64 totalSpendInUsd;
    12: i64 dailyUnits;
    13: i64 dailySpendInUsd;

    14: map<string, Creative> creatives;  
    18: map<string, list<string>> creativesBySize;
    
    15: DeliveryStats deliveryStats;

    16: double goalPriceOverride;
    17: bool override_set;
}

struct ItemInfo {
    1: string id;
    2: string name;
    
    3: FlightDates flightDates;
    4: i64 flightStart;
    5: i64 flightEnd;
    6: string timezone;
    7: Pacing pacing;
    8: PacingControl pacingControl;
    
    9: i64 totalUnits;
    10: i64 totalSpendInUsd;
    11: i64 dailyUnits;
    12: i64 dailySpendInUsd;
    13: DeliveryAllocation deliveryAllocation;
    
    14: DealType dealType;
    15: PricingType pricingType;
    16: double goalPriceInUsd;
    17: double goalCtrInPercent;
    18: double goalCpaInUsd;

    19: double averageBidInUsdCpm;
    20: double maxBidInUsdCpm;
    
    21: FeeParameters feeParameters;
    
    22: DeliveryStats deliveryStats;
    
    23: map<string, GroupInfo> groups;
    
    24: bool frequencyCapEnabled;
    25: i32 frequencyCap;
    26: i32 frequencyCapInterval;

    27: string clientId;
    28: string clientName;
    29: string clientDomain;
    
    30: string campaignId;
    31: string campaignName;
    32: string trackerId;
}
