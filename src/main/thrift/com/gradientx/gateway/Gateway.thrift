namespace java com.gradientx.gateway

include "com/gradientx/common/GradientX.thrift" 
include "com/gradientx/userdata/UserData.thrift"

enum NoBidReason {
    none,
    pacing,
    price,
    below_goal,
    gaussian,
    fee,
    competition,
    inactive,
    error,
    user_frequency
}

struct AdKey {
    1: string itemId,
    2: string groupId,
    3: string creativeId,
    4: string landerId,
    5: string clientId,
    6: string trackerId,
    7: GradientX.PricingType pricingType,
    8: bool frequencyCapEnabled,
    9: bool secondaryGoalEnabled
}

struct Ad {
    1: AdKey key
    2: string dimensions,
    3: string domain,
    4: string landerUrl,
    5: string landingPageUrl,
    6: string category,
    7: string mimeType,
    8: i32 minBitrate,
    9: i32 maxBitrate,
    10: i32 duration,
    11: string adServerId,
    12: GradientX.CreativeType type,
    14: list<GradientX.CreativeSubType> subTypes,
    15: list<GradientX.ApiFrameworks> requiredFrameworks,
    16: list<GradientX.CreativeAttributes> attributes,
    13: string markup,
    17: string campaign_id,
    18: string iUrl,
    19: string campaign_name,
    20: string item_name,
    21: string creative_name,
    22: string client_name
}

struct Bid {
	1: double price,
	2: double clickProb,
	3: double convProb,
	4: double pacingRatio
	5: double winProb,
	6: bool explore,
	7: double feePercentage,
	8: double feeMin,
	9: double feeMax,
	10: NoBidReason reason,
	11: bool selectedToBid,
	12: double winProbA,
	13: double winProbB,
	14: double winProbC,
	15: double clickProbA,
	16: double clickProbB,
	17: double clickProbC,
	18: double convProbA,
	19: double convProbB,
	20: double convProbC
}

struct RequestWrapper {
    1: string requestId,
    2: binary binarySample,
    3: list<AdKey> opportunities,
    4: UserData.UserData ud
}