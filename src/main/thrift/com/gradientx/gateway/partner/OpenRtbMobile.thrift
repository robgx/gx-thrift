namespace java com.gradientx.gateway.partner

struct OpenRtbMobileImpressionBean {
	1: string impid,
	2: i32 h,
	3: i32 w,
	4: i32 instl,
	5: set<string> btype,
	6: set<string> battr,
	7: i32 api
}

struct OpenRtbMobileAppBean {
	1: string aid,
	2: string global_aid,
	3: string name,
	4: string domain,
	5: string pid,
	6: string pub,
	7: string pdomain,
	8: set<string> cat,
	9: string keywords,
	10: string ver,
	11: string bundle,
	12: i32 paid
}

struct OpenRtbMobileSiteBean {
	1: string sid,
	2: string name,
	3: string pid,
	4: string pub,
	5: set<string> cat
}

struct OpenRtbMobileDeviceBean {
	1: string ip,
	2: string country,
	3: string carrier,
	4: string ua,
	5: string make,
	6: string model,
	7: string did,
	8: string dpid,
	9: string os,
	10: string osv,
	11: i32 js,
	12: string loc
}

struct OpenRtbMobileUserBean {
	1: string uid,
	2: i32 yob,
	3: string gender,
	4: string zip,
	5: string country,
	6: string keywords
}

struct OpenRtbMobileRestrictionsBean {
	1: set<string> bcat,
	2: set<string> badv
}

struct OpenRtbMobileBidRequestBean {
	1: string id,
	2: i32 at,
	3: i32 tmax,
	4: double pf,
	5: set<OpenRtbMobileImpressionBean> imp,
	6: OpenRtbMobileAppBean app,
	7: OpenRtbMobileSiteBean site
	8: OpenRtbMobileDeviceBean device,
	9: OpenRtbMobileUserBean user,
	10: OpenRtbMobileRestrictionsBean restrictions
}

struct OpenRtbMobileBidBean {
	1: string impid,
	2: double price,
	3: string adid,
	4: string nurl,
	5: string adm,
	6: string adomain,
	7: string iurl,
	8: string cid,
	9: string crid,
}

struct OpenRtbMobileSeatbidBean {
	1: string seat,
	2: i32 group,
	3: set<OpenRtbMobileBidBean> bid
}

struct OpenRtbMobileResultBean {
	1: string id,
	2: string bidid,
	3: string cur = "USD",
	4: i32 units = 0,
	5: set<OpenRtbMobileSeatbidBean> seatbid
}

struct OpenRtbMobileNoResultBean {
	1: string id,
	2: string bidid,
	3: i32 nbr
}