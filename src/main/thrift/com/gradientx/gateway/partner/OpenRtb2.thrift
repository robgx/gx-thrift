namespace java com.gradientx.gateway.partner

struct OpenRtb2SegmentBean {
	1: string id,
	2: string name,
	3: string value
}

struct OpenRtb2DataBean {
	1: string id,
	2: string name,
	3: set<OpenRtb2SegmentBean> segments
}

struct OpenRtb2GeoBean {
	1: double lat,
	2: double lon,
	3: string country,
	4: string region,
	5: string regionfips104,
	6: string metro,
	7: string city
	8: string zip,
	9: i32 type
}

struct OpenRtb2UserBean {
	1: string id,
	2: string buyerid,
	3: i32 yob,
	4: string gender,
	5: string keywords,
	6: string customdata,
	7: OpenRtb2GeoBean geo,
	8: set<OpenRtb2DataBean> data
}

enum ConnectionType {
	Unknown = 0,
	Ethernet = 1,
	Wifi = 2,
	Cellular = 3,
	Cellular2G = 4,
	Cellular3G = 5,
	Cellular4G = 6
}

struct OpenRtb2DeviceBean {
	1: i32 dnt,
	2: string ua,
	3: string ip,
	4: OpenRtb2GeoBean geo,
	5: string didsha1,
	6: string didmd5,
	7: string dpidsha1,
	8: string dpidmd5,
	9: string ipv6,
	10: string carrier,
	11: string language,
	12: string make,
	13: string model,
	14: string os,
	15: string osv,
	16: i32 js,
	17: ConnectionType connectiontype,
	18: i32 devicetype,
	19: string flashver
}

struct OpenRtb2ProducerBean {
	1: string id,
	2: string name,
	3: set<string> cat,
	4: string domain
}

struct OpenRtb2PublisherBean {
	1: string id,
	2: string name,
	3: set<string> cat,
	4: string domain
}

struct OpenRtb2ContentBean {
	1: string id,
	2: i32 episode,
	3: string title,
	4: string series,
	5: string season,
	6: string url,
	7: set<string> cat,
	8: i32 videoquality,
	9: string keywords,
	10: string contentrating,
	11: string userrating,
	12: string context,
	13: i32 livestream,
	14: i32 sourcerelationship,
	15: OpenRtb2ProducerBean producer,
	16: i32 len
}

struct OpenRtb2AppBean {
	1: string id,
	2: string name,
	3: string domain,
	4: set<string> cat,
	5: set<string> sectioncat,
	6: set<string> pagecat,
	7: string ver,
	8: string bundle,
	10: i32 privacypolicy,
	11: i32 paid,
	12: OpenRtb2PublisherBean publisher,
	13: OpenRtb2ContentBean content,
	14: string keywords
}

struct OpenRtb2SiteBean {
	1: string id,
	2: string name,
	3: string domain,
	4: set<string> cat,
	5: set<string> sectioncat,
	6: set<string> pagecat,
	7: string page,
	8: i32 privacypolicy,
	9: string ref,
	10: string search,
	11: OpenRtb2PublisherBean publisher,
	12: OpenRtb2ContentBean content,
	13: string keywords
}

struct OpenRtb2BannerBean {
	1: i32 w,
	2: i32 h,
	3: string id,
	4: i32 pos,
	5: set<i32> btype,
	6: set<i32> battr,
	7: set<string> mimes,
	8: i32 topframe,
	9: set<i32> expdir,
	10: set<i32> api
}

struct OpenRtb2VideoBean {
	1: set<string> mimes,
	2: i32 linearity,
	3: i32 minduration,
	4: i32 maxduration,
	5: i32 protocol,
	6: i32 w,
	7: i32 h,
	8: i32 startdelay,
	9: i32 sequence,
	10: set<i32> battr,
	11: i32 maxextended,
	12: i32 minbitrate,
	13: i32 maxbitrate,
	14: i32 boxingallowed,
	15: set<i32> playbackmethod,
	16: set<i32> delivery,
	17: i32 pos,
	18: set<OpenRtb2BannerBean> companionad,
	19: set<i32> api
}

struct OpenRtb2ImpressionBean {
	1: string id,
	2: OpenRtb2BannerBean banner,
	3: OpenRtb2VideoBean video,
	4: string displaymanager,
	5: string displaymanagerver,
	6: i32 instl,
	7: string tagid,
	8: double bidfloor,
	9: string bidfloorcur,
	10: set<string> iframebuster
}

struct OpenRtb2BidRequestBean {
	1: string id,
	2: set<OpenRtb2ImpressionBean> imp,
	3: OpenRtb2SiteBean site,
	4: OpenRtb2AppBean app,
	5: OpenRtb2DeviceBean device,
	6: OpenRtb2UserBean user,
	7: i32 at,
	8: i32 tmax,
	9: set<string> wseat,
	10: i32 allimps,
	11: set<string> cur,
	12: set<string> bcat,
	13: set<string> badv
}

struct OpenRtb2BidBean {
	1: string id,
	2: string impid,
	3: double price,
	4: string adid,
	5: string nurl,
	6: string adm,
	7: set<string> adomain,
	8: string iurl,
	9: string cid,
	10: string crid,
	11: set<i32> attr
}

struct OpenRtb2SeatbidBean {
	1: set<OpenRtb2BidBean> bid,
	2: string seat,
	3: i32 group
}

struct OpenRtb2BidResultBean {
	1: string id,
	2: set<OpenRtb2SeatbidBean> seatbid,
	3: string bidid,
	4: string cur,
	5: string customdata,
}