namespace java com.gradientx.device.thrift

struct CommonAttributes {
    1: required i32 device_type_id;
    2: optional string os_platform_code;
    3: required i32 os_platform_id;
    4: optional string os_version_code;
    5: required i32 os_version_id;
}

struct MobileDevice {
	1: required CommonAttributes common;
    2: optional string device_model_code;
    3: required i32 device_model_id;
    4: required i32 device_year_id;
    5: required i32 device_family_id;
    6: required i32 device_make_id;
    7: required i32 device_screen_size_id;
}

struct DesktopDevice {
	1: required CommonAttributes common;
	2: optional string browser_code;
    3: required i32 browser_id;
    4: optional string browser_version_code;
    5: required i32 browser_version_id;
}

struct DeviceLookups {
    1: map<string, MobileDevice> devices;
	2: map<string, i32> osPlatforms;
	3: map<string, i32> osVersions;
	4: map<string, i32> browsers;
	5: map<string, i32> browserVersions;
}