namespace java com.gradientx.user

const string HTML_AD_TYPE = "html";
const string IMAGE_AD_TYPE = "image";

struct PTDV3 {
    1: byte p,
    2: byte t,
    3: i16  d  //dayth
}

struct ProviderSegment {
    01: string id,
    02: string provider,
    03: i32    expiration,
    04: optional double confidence,
    05: optional double cost
}

struct UserData {
    01: string guid,
    02: string version,
    03: optional i32 ip,
    04: optional list<string> browser_langs,
    05: optional double lat,
    06: optional double lon,
    
    19: optional i32 minuth,
       
    /* of each hour in the last week */
    20: optional map<i32, list<i16>> impressions_per_item,
    21: optional map<i32, list<i16>> clicks_per_item,
    
    /* short-term statistics */
    30: optional i16 impressions_last_hour,
    31: optional i16 impressions_last_day,
    32: optional i16 impressions_last_week,
    
    33: optional i16 clicks_last_hour,
    34: optional i16 clicks_last_day,
    35: optional i16 clicks_last_week,
    
    /* to be calculated */
    40: optional double ctr_last_hour,
    41: optional double ctr_last_day,
    42: optional double ctr_last_week,
    
    /* long-term statistics */
    50: optional i16 impressions_last_month,
    51: optional i16 clicks_last_month,
    52: optional i16 conversion_last_month,
   
    /* to save calculation errors, ignore history of most recent 7 days of impressions and clicks that have already been saved in two lists */
    /* per day */
    60: optional list<i16> daily_impressions_last_month,
    61: optional list<i16> daily_clicks_last_month,
    62: optional list<byte> daily_conversions_last_month, 
    
    /* segments will come from multiple sources */
    70: optional list<ProviderSegment> segments
}

struct UserDataV3 {
    01: string versionTag, //"v3"
    02: string gUId,
    03: optional list<PTDV3> ptds,
    04: optional i32 ip,
    05: optional list<string> browser_langs,
    06: optional double lat,
    07: optional double lon,
    
    10: optional i32 minuth,
    
    /* of past 60 minutes sliding window */
    11: optional list<i16> bid_requests, //not exist anymore
    
    /* of each hour in the last week */
    15: optional map<i32, list<byte>> impressions_per_item,
    16: optional map<i32, list<byte>> clicks_per_item,
    
    /* short-term statistics */
    20: optional i16 impressions_last_hour,
    21: optional i16 impressions_last_day,
    22: optional i16 impressions_last_week,
    
    30: optional i16 clicks_last_hour,
    31: optional i16 clicks_last_day,
    32: optional i16 clicks_last_week,
    
    /* to be calculated */
    40: optional double ctr_last_hour,
    41: optional double ctr_last_day,
    42: optional double ctr_last_week,
    
    /* long-term statistics */
    50: optional i16 impressions_last_month,
    51: optional i16 clicks_last_month,
    52: optional i16 convertion_last_month,
    
    /* to save calculation errors, ignore history of most recent 7 days of impressions and clicks that have already been saved in two lists */
    /* per day */
    55: optional list<i16> daily_impressions_last_month,
    56: optional list<i16> daily_clicks_last_month,
    57: optional list<byte> daily_convertions_last_month, 
    
    /* segments will come from multiple sources */
    60: optional map<byte, set<string>> segments,
}
